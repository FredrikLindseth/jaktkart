# Kart over jaktterreng

Kart over jaktterrenget på [Leirnes](https://www.openstreetmap.org/#map=15/61.0855/5.8080) med jaktgrense, jaktposter, terreng fra laserscann og stier fra GPSene til jegerene.

Kartet er laget med åpne og gratis kartdata og selve kartet er laget i [QGIS](https://qgis.org/en/site/forusers/download.html)

Se seksjonen `For å lage ditt eget jaktkart` under for å lage ditt eget

![Stående jaktkart med zebrakant](jaktkart1.jpg)

![Rotert jaktkart uten zebrakant](jaktkart2.jpg)

## Oppsett for å åpne dette jaktkartet

Åpne QGIS og åpne prosjektfilen som ligger i dette repoet `./qgis/leirnes_jaktkart.qgz`

Diverse eksempelkartdata for å lage kartene vist over ligger i mappen `./qgis/`

| Kartlag            | Filnavn                     |
| ------------------ | --------------------------- |
| Høydekurver        | contour.gpkg                |
| Rutenett           | grid.gpkg                   |
| Fjellskygge        | fjellskygge.gpkg            |
| Stedsnavn          | høyanger_ssr2.geojson       |
| Jaktposter         | leirnes_jakt_poster.shp     |
| Grense jaktterreng | leirnes_jakt_grense.geojson |

Pluss noen ekstrafiler

- `viewpoint2.svg` som er ikonet til jaktpostene på kartet
- `NS3039_nordstjerne.svg`som er nordstjernen brukt

## For å lage ditt eget jaktkart

Jobben med å lage et jaktkart er tredelt

- Få tak i data/posisjon for jaktposter, stier og grenser for jaktterreng
- Stile kartdataene. Fargelegge og sette tykkelse på linjer og skriftstørrelse og farge på tekst, etc
- Selve kartet som skal skrives ut til slutt, kalt Print Layout i QGIS

Hvis man ikke har brukt QGIS før er ikke det noe problem, det finner man ut av. Jeg har også en instruksjon med bilder for å lage kart med QGIS [link her](http://0v.no/qgis/)

## Grense for jaktterreng

Lag nytt linjelag i QGIS og tegn grensene etter matrikkelen WMS. Jaktgrensene følger typisk eiendomsgrenser og Matrikkelen får man ved å legge til `https://wms.geonorge.no/skwms1/wms.matrikkel.v1?request=GetCapabilities&service=WMS` som en kartjeneste i QGIS som beskrevet [her](http://0v.no/qgis/#legge-til-nye-karttjenester-wmstmswfs)

Fargelegg grensen rød og sett den til 1 mm brei ved å åpne menyen for å stile dataene må man høyreklikke på datalaget i boksen nede til venstre `Layers`, velg `Properties` og så gå til `Symbology`

## Jaktposter

Lag nytt punktlag i QGIS og plasser punkter på plassene hvor det er jaktposter, legg til ett par datafelt per punkt som beskrevet [link til instruksjon](http://0v.no/qgis/#legg-til-datafelt-i-laget). De trenger `navn` og `rotasjon`

For å åpne menyen for å stile dataene må man høyreklikke på datalaget i boksen nede til venstre `Layers, velg Properties og så gå til Symbology

Stil post-punktene som et SVG Marker med et ikon (`jaktkart/qgis/viewpoint2.svg` her i repoet) og gjør det 80 Map Units

Sett `Rotation` til å bruke "retning"-feltet på jaktpostene

Legg til en Label, sett skriftfargen / fill colour til hvit, skriftstørrelsen til 16 og `placement` til `Cartographic` og `Distance Offset from` til `From Symbol Bounds`

## Stier og skogsveier

Hent ut stier (`highway=path`) og skogsveier (`highway=track`) fra OpenStreetMap via [overpass turbo](https://overpass-turbo.eu/s/WPE). Kjør spørringen og klikk på `Export` og `Download GeoJSON`. Dra filen inn i QGIS

```overpass-ql
[out:json][timeout:25];
{{geocodeArea:høyanger}}->.searchArea;
(
  way["highway"="track"](area.searchArea);
  way["highway"="path"](area.searchArea);
);
out body;
>;
out skel qt;
```

Er det stier som ikke er i kartdataene kan man enten tegne de inn på [openstreetmap](https://www.openstreetmap.org/edit) for så å hente ut via overpass eller bruke GPXfiler fra med GPS.

## Stedsnavn

Det enkleste for å få stedsnavn på et brukbart format er å laste ned fra openstreetmap igjen. Last ned importfiler for den ønskede kommunen her [Sentralt Stedsnavn Register (SSR)](https://obtitus.github.io/ssr2_to_osm_data/)

Finn kommunen og last ned filen `Dataset for import`-kolonnen som inneholder navn på alt.

Dra filen inn i qgis og stil laget som de andre. Dette laget bør være et F

## Elver, vann og myrer

Hent ut `natural=water` (vann), `natural=waterway` (elver) og `natural=wetland` (myr) fra [overpass turbo](https://overpass-turbo.eu)

```overpass_QL
[out:json][timeout:25];
{{geocodeArea:Høyanger}}->.searchArea;
(
  nwr["natural"="water"](area.searchArea);
  nwr["natural"="waterway"](area.searchArea);
  nwr["natural"="wetland"](area.searchArea);
);
out body;
>;
out skel qt;
```

Hvis man ønsker andre elementer enn elver skog og vann så kan man se i listen [her](https://wiki.openstreetmap.org/wiki/Key:natural) og så putte de inn i spørringen til Overpass

## Få tak i høydedata

1. Last ned DTM1-høydedata fra [Høydedata.no](https://hoydedata.no/LaserInnsyn/?x=5354&y=6807209&level=13&utm=33&projects=&layers=&raster=&background=wmtsTopoGraatone) [link til instruksjon](http://0v.no/qgis/#data-fra-h%C3%B8ydedatano)
2. Pakk ut og dra filene, i data-mappen du pakket ut, inn i QGIS
3. Slå sammen de ulike høydedatalagene og lag en virtual raster [link til instruksjon](http://0v.no/qgis/#virtual-raster)
4. Høyreklikk på det virtuelle laget og dupliser det. La det ene laget være og fargelegg det andre som hillshade [link til instruksjon](http://0v.no/qgis/#fjellskygge)
5. extract countour / høydekurver. 1 kurve per 1 meter er fint [link til instruksjon](http://0v.no/qgis/#h%C3%B8ydekurver)

## Rutenett

For å lage et rutenett som dekker kartet klikk på menyen øverst i QGIS :

`Vector` - `Research Tools` - `Grids`

Lag et rutenett med 250 meter horisontal og vertikal spacing, velg Grid Extent fra fra et av de andre kartlagene

## Gjøre alt klart

Rett rekkefølge på lagene. De øverste tegner over de under så ha fjellskygge nederst, så høydekurver og grid og så alt annet over det

## Lag en PDF med kartet

Trykk CTRL+P for å gå til Print Layout hvor man lager den endelige PDFen

Lag print layout som beskrevet i [instruksjonen her](http://0v.no/qgis/#print-layout)

For å legge til rutenett og zebrakant rundt

- klikk på kartet i print layout
- Item `Properties` - `Grids`
- Trykk på plussen og setter `Interval` til 250m, 250m

Zebra frame-kanten rundt, settes også inne i Grids

- klikk på kartet i print layout
- Item Propertie - Frame
- Frame style - Zebra
- Left side, Top SIde, 5mm tykk
- Offset zebrakanten så den passer med grid

- Putt på tekstmarkører med ABCDEFGH på zebrabaren oppe og 1234567 nedover

For å ha nordstjerne [link til instruksjon](http://0v.no/qgis/#hvordan-lage-kompassrose)

For en målestokk [link til instruksjon](http://0v.no/qgis/#hvordan-lage-m%C3%A5lestokken)

## Rotasjon av kartet

Hvis man ønsker å rotere kartet f.eks for å passer bedre på et ark må man klikke på kartet i print layout- `Main Properties` - `Map rotation`. Eksempelkartet øverst er rotert 60 grader

`NS3039_nordstjerne.svg` er brukt til å indikere nord i kartet som er rotert.

## Infoboks i på papirkartet

Tanken slo meg at hvis kartet henger på post så kunne det vært nyttig med skyteavstande fra postene til gitte mål, men dette har jeg ikke fått lagt inn enda.

Avstad i meter fra Bøen til mål

|                           |     |
| ------------------------- | --- |
| porten man går inn        | 75  |
| fjellet oppe i nord       | 100 |
| hjørnet av grunnmuren     | 60  |
| strømmasten oppe i skogen | 125 |
| trimløypen i skoegn       | 35  |
